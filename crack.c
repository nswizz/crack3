#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char word[25];
    char *hash;
};

int compare(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa->hash, bb->hash);
    return strcmp(aa->word, bb->word);
}
int hashcompare(const void *input, const void *check)
{
    char *target = (char *)input;
    struct entry *checks = (struct entry *)check;
    return strcmp(target, checks->hash);
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *words = fopen(filename,"r");
    char line[100];
    int count = 0;
    *size = 50;
    struct entry *input = malloc(*size * sizeof(struct entry));
    while(fgets(line, 100, words) != NULL)
    {
        if(count == *size)
        {
            *size += 50;
            input = realloc(input, *size * sizeof(struct entry));
        }
        line[strlen(line)-1] = '\0';
        strcpy(input[count].word,line);
        char *hashes = md5(line, strlen(line));
        input[count].hash = hashes;
        count++;
    }
    *size = count;
    //fprintf(stderr,"%s %s\n", input[0].hash, input[0].word);
    fclose(words);
    return input;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    //FILE *hash = fopen(argv[2],"r");
    // TODO: Read the dictionary file into an array of entry structures
    int length;
    struct entry *dict = read_dictionary(argv[2], &length);
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    // qsort(dict, 0, 0, NULL);
    qsort(dict, length, sizeof(struct entry), compare);
    
    for(int i = 0; i < length; i++)
    {
        //fprintf(stderr, "%s %s\n", dict[i].word, dict[i].hash);
    }
    
    // TODO
    // Open the hash file for reading.
    FILE *check = fopen(argv[1], "r");
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char line[100];
    while(fgets(line, 100, check) != NULL)
    {
        line[strlen(line)-1] = '\0';
        //fprintf(stderr, "%s\n",line);
        struct entry *found = bsearch(line, dict, length, sizeof(struct entry), hashcompare);
        if(found != NULL)
        {
            printf("Match: %s %s\n", found->word, found->hash);
        }
    }
    free(dict);
    fclose(check);
}
